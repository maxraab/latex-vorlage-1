
[![build status](https://gitlab.com/maxraab/latex-vorlage-1/badges/master/build.svg)](https://gitlab.com/maxraab/latex-vorlage-1/commits/master)


Dieses Projekt dient dem Experimentieren mit LaTeX.


Die aktuelle PDF kann 31 Tage lang [hier gedownloaded werden](https://gitlab.com/maxraab/latex-vorlage-1/builds/artifacts/master/download?job=pdf)